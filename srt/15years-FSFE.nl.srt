1
00:00:02,786 --> 00:00:05,817
Wist je dat jouw mobiele telefoon meer rekenkracht heeft

2
00:00:05,817 --> 00:00:07,974
dan de computers die astronauten op de maan hebben gezet?

3
00:00:07,974 --> 00:00:10,273
Daarmee heb je veel mogelijkheden in jouw hand.

4
00:00:10,273 --> 00:00:12,503
Vandaag de dag zijn er overal computers 

5
00:00:12,503 --> 00:00:14,854
en de software die er op draait doet geweldige dingen:

6
00:00:14,854 --> 00:00:17,712
Zij stuurt de telefoons aan waarmee we in contact blijven,

7
00:00:17,712 --> 00:00:19,430
auto's, die ons ergens heen brengen,

8
00:00:19,430 --> 00:00:21,753
en pacemakers, die mensen in leven houden.

9
00:00:21,753 --> 00:00:25,000
Maar soms beperkt software je door het censureren van berichten

10
00:00:25,000 --> 00:00:28,119
of door te voorkomen dat je media deelt met vrienden.

11
00:00:28,119 --> 00:00:31,274
Wanneer je niet kan zien of kan veranderen hoe software werkt, 

12
00:00:31,274 --> 00:00:33,773
verlies je de controle over jouw data en jouw privacy.

13
00:00:33,773 --> 00:00:36,000
We geloven dat het recht om software te gebruiken, te bestuderen,

14
00:00:36,000 --> 00:00:39,245
te delen en te wijzigen essentieel is voor vrijheid.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
maakt <i>jouw</i> technologische zelfbeschikking mogelijk. 

17
00:00:45,994 --> 00:00:59,182
De FSFE heeft meer dan 15 jaren voor jouw rechten gevochten. Doe met ons mee en 
verdedig je vrijheden in de komende jaren.
