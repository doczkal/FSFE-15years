1
00:00:02,786 --> 00:00:05,817
Da li ste znali da Vaš mobilni telefon ima više procesorske snage

2
00:00:05,817 --> 00:00:07,974
nego računari koji su odveli astronaute na mesec?

3
00:00:07,974 --> 00:00:10,273
To je puno moći u vašim rukama. 

4
00:00:10,273 --> 00:00:12,503
Računari su danas svuda oko nas

5
00:00:12,503 --> 00:00:14,854
i softver koji ih pokreće čini neverovatne stvari:

6
00:00:14,854 --> 00:00:17,712
Pokreće telefone koje koristimo da bi ostali u kontaktu,

7
00:00:17,712 --> 00:00:19,430
automobile, koji nas dovode do željenih destinacija,

8
00:00:19,430 --> 00:00:21,753
i pejsmejkere, koji održavaju ljude živim. 

9
00:00:21,753 --> 00:00:25,000
Ali ponekad, softver te ograničava tako što cenzuriše poruke

10
00:00:25,000 --> 00:00:28,119
ili tako što te sprečava da deliš multimediju sa prijateljima.

11
00:00:28,119 --> 00:00:31,274
Kada ne možeš da vidiš ili promeniš kako softver radi, 

12
00:00:31,274 --> 00:00:33,773
ti gubiš kontrolu nad svojim podacima i nad svojom privatnošću.

13
00:00:33,773 --> 00:00:36,000
Mi verujemo da je pravo da se koristi, proučava,

14
00:00:36,000 --> 00:00:39,245
deli, i prilagođava softver neophodno za slobodu.

15
00:00:39,245 --> 00:00:41,090
Fondacija za slobodni softver Evrope

16
00:00:41,090 --> 00:00:44,611
Zadržava moć tehnologije u <i>vašim</i> rukama. 

17
00:00:45,994 --> 00:00:59,182
FSFE se bori za vaša prava više od 15 godina. Pridruži nam se u odbrani slobode u godinama što dolaze. 
