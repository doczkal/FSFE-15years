1
00:00:02,786 --> 00:00:05,817
Kas sa teadsid, et sinu mobiiltelefoni andmetöötlusvõimsus on suurem

2
00:00:05,817 --> 00:00:07,974
kui arvutitel, mis saatsid astronaudid kuule?

3
00:00:07,974 --> 00:00:10,273
See tähendab, et sinu käes on palju võimu.

4
00:00:10,273 --> 00:00:12,503
Arvutid ümbritsevad meid kõikjal

5
00:00:12,503 --> 00:00:14,854
ja nendel töötava tarkvaraga on võimalik teha imelisi asju:

6
00:00:14,854 --> 00:00:17,712
see töötab telefonidel, mis võimaldavad meil hoida ühendust

7
00:00:17,712 --> 00:00:19,430
autodel, mis viivad meid kohtadesse

8
00:00:19,430 --> 00:00:21,753
ja südamerütmuritel, mis hoiavad meid elus.

9
00:00:21,753 --> 00:00:25,000
Kuid vahepeal tarkvara piirab sind, tsenseerides sinu sõnumeid

10
00:00:25,000 --> 00:00:28,119
või keelates sul jagada meediat sõpradega.

11
00:00:28,119 --> 00:00:31,274
Kui sa ei näe, kuidas tarkvata töötab, või ei saa seda muuta,

12
00:00:31,274 --> 00:00:33,773
siis kaotad kontrolli oma andmete ja privaatsuse üle.

13
00:00:33,773 --> 00:00:36,000
Me usume, et õigus kasutada, õppida,

14
00:00:36,000 --> 00:00:39,245
jagada, ja kohandada tarkvara on vabaduse põhialus. 

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Hoiab tehnoloogia võimu <i>sinu</i> kätes.

17
00:00:45,994 --> 00:00:59,182
FSFE on võidelnud sinu õiguste eest üle 15 aasta. Ühine meiega oma vabaduste eest seismises ka edaspidi.

