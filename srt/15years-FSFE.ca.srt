1
00:00:02,786 --> 00:00:05,817
¿Saps que el teu telèfon mòbil té més poder de processament

2
00:00:05,817 --> 00:00:07,974
que els ordinadors que van fer arribar als astronautes a la Lluna?

3
00:00:07,974 --> 00:00:10,273
Això és molt de poder en les teves mans.

4
00:00:10,273 --> 00:00:12,503
Avui els ordinadors estan per tot arreu

5
00:00:12,503 --> 00:00:14,854
i el programari que executen fa coses sorprenents:

6
00:00:14,854 --> 00:00:17,712
funciona en els mòbils que utilitzem per estar en contacte

7
00:00:17,712 --> 00:00:19,430
en els cotxes que ens porten a llocs,

8
00:00:19,430 --> 00:00:21,753
i els marcapasos que mantenen en vida a la gent.

9
00:00:21,753 --> 00:00:25,000
Tanmateix, el programari et limita censurant missatges

10
00:00:25,000 --> 00:00:28,119
o evitant que comparteixis fitxers amb amics.

11
00:00:28,119 --> 00:00:31,274
Quan no pots veure o canviar el funcionament del programari,

12
00:00:31,274 --> 00:00:33,773
perds el control de les teves dades i la teva privacitat.

13
00:00:33,773 --> 00:00:36,000
Creiem en el dret d'utilitzar, estudiar,

14
00:00:36,000 --> 00:00:39,245
compartir i adaptar el programari és essencial per la llibertat.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Mantenint el poder de la tecnologia en les <i>teves</i> mans.

17
00:00:45,994 --> 00:00:59,182
La FSFE ha estat lluitant pels teus drets més de 15 anys. Uneix-te a nosaltres en la defensa de les teves llibertats en els pròxims anys.
