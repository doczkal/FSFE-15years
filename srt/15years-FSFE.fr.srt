1
00:00:02,786 --> 00:00:05,817
Saviez-vous que votre téléphone mobile a plus de puissance de traitement                                         

2
00:00:05,817 --> 00:00:07,974
que les ordinateurs ayant servi à envoyer les astronautes sur la lune?

3
00:00:07,974 --> 00:00:10,273
Cela fait beaucoup de puissance entre vos mains.


4
00:00:10,273 --> 00:00:12,503
Aujourd'hui, les ordinateurs sont partout

5
00:00:12,503 --> 00:00:14,854
et les logiciels s'exécutant dessus font des choses fantastiques:


6
00:00:14,854 --> 00:00:17,712
Ils font fonctionner les téléphones que nous utilisons pour rester en contact,

7
00:00:17,712 --> 00:00:19,430
les voitures qui nous transportent,  

8
00:00:19,430 --> 00:00:21,753
et les stimulateurs cardiaques, qui maintiennent des gens en vie.

9
00:00:21,753 --> 00:00:25,000
Mais parfois, le logiciel vous limite en censurant des messages,

10
00:00:25,000 --> 00:00:28,119
ou en vous empêchant de partager des médias avec des amis.

11
00:00:28,119 --> 00:00:31,274
Lorsque vous ne pouvez pas voir comment fonctionne le logiciel ou le modifier,  

12
00:00:31,274 --> 00:00:33,773
vous perdez le contrôle de vos données et votre vie privée.

13
00:00:33,773 --> 00:00:36,000
Nous pensons que le droit d’utiliser, étudier,

14
00:00:36,000 --> 00:00:39,245
partager et adapter les logiciels est essentiel à la liberté.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Gardez le pouvoir de la technologie entre <i>vos</i> mains.

17
00:00:45,994 --> 00:00:59,182
La FSFE se bat pour vos droits depuis plus de 15 ans. Rejoignez-nous pour la défense de nos libertés pour les années à venir.
