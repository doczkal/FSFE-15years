1
00:00:02,786 --> 00:00:05,817
Cep telefonunuzun, astronotları aya gönderen bilgisayarlardan

2
00:00:05,817 --> 00:00:07,974
daha fazla işlem gücüne sahip olduğunu biliyor musunuz?

3
00:00:07,974 --> 00:00:10,273
Bu, elinizde bir çok güç olduğuna anlamına geliyor.

4
00:00:10,273 --> 00:00:12,503
Bugün bilgisayarlar her yerde

5
00:00:12,503 --> 00:00:14,854
ve üzerlerinde çalışan yazılımlar muhteşem şeyler yapıyor:

6
00:00:14,854 --> 00:00:17,712
Temasta kalmamazı sağlayan telefonları, bizi 

7
00:00:17,712 --> 00:00:19,430
bir yerlere götüren arabaları,

8
00:00:19,430 --> 00:00:21,753
ve insanları hayatta tutan kalp pillerini çalıştırıyor.

9
00:00:21,753 --> 00:00:25,000
Ancak bazen, yazılımlar mesajlarınızı sansürleyerek veya

10
00:00:25,000 --> 00:00:28,119
arkadaşlarınızla medya paylaşmanızı engelleyerek sizi kısıtlar.

11
00:00:28,119 --> 00:00:31,274
Yazılımların nasıl çalıştığını göremez veya değiştiremezseniz,

12
00:00:31,274 --> 00:00:33,773
verilerinizin ve mahremiyetinizin denetimini yitirirsiniz.

13
00:00:33,773 --> 00:00:36,000
Yazılımı kullanma, ınceleme, paylaşma ve uyarlama

14
00:00:36,000 --> 00:00:39,245
hakkının özgürlük için asli olduğunu düşünüyoruz.

15
00:00:39,245 --> 00:00:41,090
Avrupa Özgür Yazılım Vakfı (FSFE)

16
00:00:41,090 --> 00:00:44,611
Teknolojinin gücünü <i>kendi</i> ellerinizde tutun.

17
00:00:45,994 --> 00:00:59,182
FSFE, 15 yıldan fazladır haklarınız için mücadele ediyor. Önümüzdeki yıllarda özgürlüklerinizi savunmamızda bize katılın.
