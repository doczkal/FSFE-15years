1
00:00:02,786 --> 00:00:05,817
¿Sabes que tu móvil tiene más poder de procesamiento

2
00:00:05,817 --> 00:00:07,974
que los ordenadores que pusieron astronautas en la luna?

3
00:00:07,974 --> 00:00:10,273
Eso es un montón de poder en tus manos.

4
00:00:10,273 --> 00:00:12,503
Hoy los ordenadores están en todas partes

5
00:00:12,503 --> 00:00:14,854
y el software que ejecutan hace cosas asombrosas:

6
00:00:14,854 --> 00:00:17,712
funciona en los móviles que usamos para estar en contacto,

7
00:00:17,712 --> 00:00:19,430
en los coches que nos llevan a sitios,

8
00:00:19,430 --> 00:00:21,753
y en los marcapasos que mantienen viva a la gente

9
00:00:21,753 --> 00:00:25,000
Pero a veces, el software te limita censurando mensajes

10
00:00:25,000 --> 00:00:28,119
o evitando que compartas archivos con amigos.

11
00:00:28,119 --> 00:00:31,274
Cuando no puedes ver o cambiar el funcionamiento del software,

12
00:00:31,274 --> 00:00:33,773
pierdes el control de tus datos y tu privacidad.

13
00:00:33,773 --> 00:00:36,000
Creemos que el derecho a usar, estudiar,

14
00:00:36,000 --> 00:00:39,245
compartir y adaptar el software es esencial para la libertad.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Manteniendo el poder de la tecnología en <i>tus</i> manos.

17
00:00:45,994 --> 00:00:59,182
La FSFE ha estado luchando por tus derechos más de 15 años. Únete a nosotros en la defensa de tus libertades en los próximos años.

