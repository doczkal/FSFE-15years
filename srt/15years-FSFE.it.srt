1
00:00:02,786 --> 00:00:05,817
Lo sapevi che il tuo telefono cellulare ha più potenza di calcolo

2
00:00:05,817 --> 00:00:07,974
dei computer che portarono gli astronauti sulla luna?

3
00:00:07,974 --> 00:00:10,273
Questo è un grande potere nelle tue mani.

4
00:00:10,273 --> 00:00:12,503
Oggigiorno i computer sono ovunque

5
00:00:12,503 --> 00:00:14,854
ed il software che esegue su di essi fa cose incredibili:

6
00:00:14,854 --> 00:00:17,250
fa funzionare il cellulare che usiamo per rimanere in contatto,

7
00:00:17,712 --> 00:00:19,250
le auto che ci trasportano,

8
00:00:19,250 --> 00:00:21,250
e i pacemaker che mantengono le persone in vita.

9
00:00:21,900 --> 00:00:25,000
Ma qualche volta, il software ti limita censurando messaggi

10
00:00:25,000 --> 00:00:28,119
o impedendoti di condividere i media con amici.

11
00:00:28,119 --> 00:00:31,274
Quando non sei in grado di vedere o cambiare come il software funziona,

12
00:00:31,274 --> 00:00:33,773
perdi il controllo dei tuoi dati e della tua privacy.

13
00:00:33,773 --> 00:00:36,000
Noi crediamo che il diritto di usare, studiare,

14
00:00:36,000 --> 00:00:39,245
condividere, e adattare il software sia essenziale per la libertà.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Mantenendo il potere della tecnologia nelle <i>tue</i> mani.

17
00:00:45,994 --> 00:00:59,182
La FSFE si sta battendo per i tuoi diritti da più di 15 anni. Unisciti a noi nel difendere le tue libertà negli anni a venire.

