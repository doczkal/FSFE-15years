1
00:00:02,786 --> 00:00:05,817
Did you know your mobile phone has more processing power

2
00:00:05,817 --> 00:00:07,974
than the computers that put astronauts on the moon?

3
00:00:07,974 --> 00:00:10,273
That's a lot of power in your hands.

4
00:00:10,273 --> 00:00:12,503
Computers are everywhere today

5
00:00:12,503 --> 00:00:14,854
and the software running on them does amazing things:

6
00:00:14,854 --> 00:00:17,712
It runs the phones we use to stay in touch,

7
00:00:17,712 --> 00:00:19,430
cars, that take us places,

8
00:00:19,430 --> 00:00:21,753
and pacemakers, that keep people alive.

9
00:00:21,753 --> 00:00:25,000
But sometimes, software restricts you by censoring messages

10
00:00:25,000 --> 00:00:28,119
or by preventing you from sharing media with friends.

11
00:00:28,119 --> 00:00:31,274
When you can't see or change how software works,

12
00:00:31,274 --> 00:00:33,773
you lose control of your data and your privacy.

13
00:00:33,773 --> 00:00:36,000
We believe that the right to use, study,

14
00:00:36,000 --> 00:00:39,245
share, and adapt software is essential to liberty.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Keeping the power of technology in <i>your</i> hands.

17
00:00:45,994 --> 00:00:59,182
The FSFE has been fighting for your rights for over 15 years. Join us in defending your freedoms in the years to come.

