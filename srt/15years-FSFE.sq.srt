1
00:00:02,786 --> 00:00:05,817
A e dini se celulari juaj ka më tepër fuqi përpunimi

2
00:00:05,817 --> 00:00:07,974
se sa kompjuterat që drejtuan astronautët në Hënë?

3
00:00:07,974 --> 00:00:10,273
Pra bëhet fjalë për shumë fuqi në duart tuaja.

4
00:00:10,273 --> 00:00:12,503
Kompjuterat sot gjenden kudo

5
00:00:12,503 --> 00:00:14,854
dhe software-i që xhiron në to bën gjëra mahnitëse:

6
00:00:14,854 --> 00:00:17,712
Vë në punë telefonat që përdorim për t’u lidhur,

7
00:00:17,712 --> 00:00:19,430
makinat, që na shpien diku,

8
00:00:19,430 --> 00:00:21,753
dhe stimulatorët kardiakë, që i mbajnë njerëzit në jetë.

9
00:00:21,753 --> 00:00:25,000
Por ndonjëherë software-i ju kufizon, duke censuruar mesazhet

10
00:00:25,000 --> 00:00:28,119
ose duke ju penguar të ndani media me miqtë.

11
00:00:28,119 --> 00:00:31,274
Kur s’e shihni ose ndryshoni dot mënyrën se si punon software-i,

12
00:00:31,274 --> 00:00:33,773
e humbni kontrollin mbi të dhënat tuaja dhe privatësinë.

13
00:00:33,773 --> 00:00:36,000
Ne besojmë se e drejta për të përdorur, studiuar,

14
00:00:36,000 --> 00:00:39,245
ndarë me të tjerët, dhe përshtatur software-in është thelbësore për lirinë.

15
00:00:39,245 --> 00:00:41,090
Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
Për mbajtjen e fuqisë së teknologjisë në duart <i>tuaja</i>.

17
00:00:45,994 --> 00:00:59,182
FSFE-ja ka luftuar për të drejtat tuaja për më shumë se 15 vjet. Ejani me ne për mbrojtjen e lirive tuaja në vitet që do të vijnë.

