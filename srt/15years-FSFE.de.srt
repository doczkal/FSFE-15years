1
00:00:02,786 --> 00:00:05,817
Wusstest du, dass dein Handy mehr Rechenleistung hat

2
00:00:05,817 --> 00:00:07,974
als die Computer, die Astronauten auf den Mond brachten?

3
00:00:07,974 --> 00:00:10,273
Damit hältst du viele Möglichkeiten in Händen.

4
00:00:10,273 --> 00:00:12,503
Computer sind heutzutage überall

5
00:00:12,503 --> 00:00:14,854
und die Software, die darauf läuft, leistet fantastische Dinge:

6
00:00:14,854 --> 00:00:17,712
Sie betreibt Telefone, mit denen wir in Kontakt bleiben,

7
00:00:17,712 --> 00:00:19,430
Autos, die uns zu anderen Orten bringen

8
00:00:19,430 --> 00:00:21,753
und Herzschrittmacher, die Menschen am Leben halten.

9
00:00:21,753 --> 00:00:25,000
Aber manchmal schränkt Software dich ein, indem sie Nachrichten zensiert

10
00:00:25,000 --> 00:00:28,119
oder dich daran hindert, Medien mit Freunden zu teilen.

11
00:00:28,119 --> 00:00:31,274
Wenn du nicht sehen oder ändern kannst wie Software funktioniert,

12
00:00:31,274 --> 00:00:33,773
verlierst du die Kontrolle über deine Daten und deine Privatsphäre.

13
00:00:33,773 --> 00:00:36,000
Wir glauben, dass das Recht Software zu verwenden, zu verstehen,

14
00:00:36,000 --> 00:00:39,245
zu teilen und zu verändern eine Grundvoraussetzung für Freiheit ist.

15
00:00:39,245 --> 00:00:41,090
Die Free Software Foundation Europe

16
00:00:41,090 --> 00:00:44,611
ermöglicht <i>dir</i> den selbstbestimmten Umgang mit Technik.

17
00:00:45,994 --> 00:00:59,182
Die FSFE kämpft seit über 15 Jahren für deine Rechte. Hilf uns dabei deine Freiheit in den kommenden Jahren zu verteidigen.

